# Generated by Django 3.2.12 on 2023-05-16 09:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactmodel',
            name='mobile',
            field=models.IntegerField(),
        ),
    ]
