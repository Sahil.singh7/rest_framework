from django.shortcuts import render
from .models import *
from .serializer import *
from rest_framework import viewsets

# Create your views here.

class ContactViewSet(viewsets.ModelViewSet):
    queryset=ContactModel.objects.all()
    serializer_class=ContactSerializer
    