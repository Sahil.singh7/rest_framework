from rest_framework import serializers
from .models import *

class ContactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ContactModel
        fields=('name','email','mobile','address')


